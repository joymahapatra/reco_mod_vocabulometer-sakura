import json,os

def file_to_sents(file):
    """assuming file is in /data/id_files/"""
    f = open("data/id_files/"+file, "r") # file =
    j_text = json.loads(f.read())
    body_doc = j_text['body']

    text = ""
    for p in body_doc: 		 				#list of paragraphs, each paragraph is a dict of 'interWords' 'words'
        l_words = p['words'] 				#list of words of this paragraph
        i_words = p['interWords']
        count = 0
        text += i_words[count]
        for w in l_words:					#for each word of this paragraph
            text += w['raw']
            count += 1
            text += i_words[count]
    sentence_list = text.split(".")[:-1]
    for k in range(len(sentence_list)):
            sentence_list[k] = sentence_list[k].lower()
    return sentence_list

if __name__ == "__main__":

    for i in open("data/doc_IDs.txt", "r"):
        if i[:-1]+".txt" in os.listdir("data/id_files"):
            print(file_to_sents(i[:-1]+".txt"))
