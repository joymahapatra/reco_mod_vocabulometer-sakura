#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import torch, numpy as np
from infersent_models import InferSent

class Encoding:
    """infersent encoder : https://github.com/facebookresearch/InferSent"""

    def __init__(self):
        """initialization"""
        V = 2
        MODEL_PATH = 'encoder/infersent%s.pkl' % V
        params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                        'pool_type': 'max', 'dpout_model': 0.0, 'version': V}
        self.infersent = InferSent(params_model)
        self.infersent.load_state_dict(torch.load(MODEL_PATH))
        W2V_PATH = 'fastText/crawl-300d-2M.vec'
        self.infersent.set_w2v_path(W2V_PATH)
        self.infersent.build_vocab_k_words(K=500000)

    def paragraph_to_vector(self, sentence_list):
        """encode a paragraph"""
        for k in range(len(sentence_list)):
            sentence_list[k] = sentence_list[k].lower()
        return np.mean(self.infersent.encode(sentence_list), axis=0)

    def paragraph_to_keyword(self, sentence_list, top_k=10):
        """extract keywords from a paragraph"""
        for k in range(len(sentence_list)):
            sentence_list[k] = sentence_list[k].lower()
        return self.infersent.valued_word(sentence_list, top_k)

    def word_to_vec(self, word):
        """word to vector"""
        word = word.lower()
        if word in self.infersent.word_vec:
            return(self.infersent.word_vec[word])
        else:
            print("[slight warning] We have just encounter a unknown word, so you may increase the vocabulary size.")
            return(self.infersent.word_vec['unknown'])

    def sent_to_vec(self, sent):
        """sentence to vector"""
        sent_list = sent.split()
        print(sent_list)

        sent_addition = np.zeros((300,))
        sent_hadamard = np.ones((300,))
        count = 0
        for i in sent_list:
            tmp = self.word_to_vec(i)
            sent_addition += tmp
            sent_hadamard *= tmp
            count += 1

        sent_average = sent_addition/count

        return np.concatenate((sent_addition, sent_hadamard, sent_average)) # 900



if __name__ == "__main__":
    """testing portion :)"""
    encoder = Encoding()
    print(encoder.paragraph_to_keyword([" ", "Word based text encoding to keep track of used vocabularies in that text"]))#,"Word based text encoding to keep track of used vocabularies in that text","my name is joy","This partbsize=64, tokenize=True, verbose=False considers only words inside of a document bsize=64, tokenize=True, verbose=False bsize=64, tokenize=True, verbose=False"]))




