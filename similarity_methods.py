#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from nltk.stem import WordNetLemmatizer
from itertools import chain
from nltk.corpus import wordnet
import numpy as np

def synonyms(word):
    # you can also use .hyponyms(), .hypernyms(), .definition()
    """https://stackoverflow.com/a/35371810"""
    synonyms = wordnet.synsets(word)
    lemmas = set(chain.from_iterable([word.lemma_names() for word in synonyms]))
    return lemmas


stopword = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves",
                "you", "your", "yours", "yourself", "yourselves", "he", "him",
                "his", "himself", "she", "her", "hers", "herself", "it", "its",
                "itself", "they", "them", "their", "theirs", "themselves",
                "what", "which", "who", "whom", "this", "that", "these", "those",
                "am", "is", "are", "was", "were", "be", "been", "being", "have",
                "has", "had", "having","do", "does", "did", "doing", "a", "an",
                "the", "and", "but", "if", "or", "because", "as", "until",
                "while", "of", "at", "by", "for", "with", "about","against",
                "between", "into", "through", "during", "before", "after", "above",
                "below", "to", "from", "up", "down", "in", "out", "on", "off", "over",
                "under", "again", "further", "then", "once", "here", "there",
                "when", "where", "why", "how", "all", "any", "both", "each", "few",
                "more", "most", "other", "some", "such", "no", "nor", "not", "only",
                "own", "same", "so", "than", "too", "very", "s", "t", "can", "will",
                "just", "don", "should", "now"
                ]


def jaccard_similarity(string_1, string_2):
    x = string_1.lower().split()
    y = string_2.lower().split()
    lemmatizer = WordNetLemmatizer()
    x_ = []
    y_ = []
    for i in range(len(x)):
        x[i] = lemmatizer.lemmatize(x[i])
        if x[i] not in stopword:
            x_.append(x[i])
    for i in range(len(y)):
        y[i] = lemmatizer.lemmatize(y[i])
        if y[i] not in stopword:
            y_.append(y[i])
    x = x_
    y = y_
    intersection_cardinality = len(set.intersection(set(x), set(y)))
    union_cardinality = len(set.union(set(x), set(y)))
    print(intersection_cardinality, union_cardinality)
    if union_cardinality == 0:
        union_cardinality = 1
    return float(intersection_cardinality)/(float(union_cardinality))


def cosine_similarity(a, b):
    return np.dot(a, b)/(np.norm(a)*np.norm(b))


