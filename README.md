# Recommendation Module for Vocabulometer

* The proposed recommendation strategy is based on the user's past reading details.
* Unlike traditional IR based models (e.g. tf-idf scoring), here SOTA based document/sentence embedding methods are used in this code.
* While handling text embeddings, Facebook's Infersent model is incorporated. Please look into their [github page](https://github.com/facebookresearch/InferSent), for details.

### Directory Structure
> * [data/](data/) : Contains document ids ([file](data/doc_IDs.txt)) and file corresponding to the ids.
> * [data/id_files](data/id_files/) : Contains file corresponding to each ids.
> * [encoder/](encoder/) : Contains pretrained Facebook infersent models.
> * [fastText/](fastText/) : Contains pretrained word vector file (dimention of each word is 300).
> * [extract_features.py](extract_features.py) : This file comes with [Facebook Infersent](https://github.com/facebookresearch/InferSent).
> * [infersent_models.py](infersent_models.py) : This file comes with [Facebook Infersent](https://github.com/facebookresearch/InferSent) and named as `models.py`
> * [file_processing.py](file_processing.py) : Reads data from [data/](data/).
> * [text_encoder.py](text_encoder.py) : encodes texts into their encoding.
> * [run.py](run.py) : execution module. 

### Examples
* 

### Reference Links
* [Facebook Infersent](https://github.com/facebookresearch/InferSent)
* [fastText](https://fasttext.cc)
* [GloVe](https://nlp.stanford.edu/projects/glove/)
* [Vocabulometer](http://vocabulometer.herokuapp.com/)